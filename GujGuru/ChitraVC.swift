//
//  ChitraVC.swift
//  GujGuru
//
//  Created by Nick Joliya on 12/10/23.
//
import UIKit

class ChitraVC: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var viewCanvas: UIView!
    @IBOutlet weak var viewDrawPad: CanvasView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        scrollView.delegate = self
        scrollView.isScrollEnabled = false
        scrollView.minimumZoomScale = 0.5
        scrollView.maximumZoomScale = 10.0
        strokeWidth = 3.0
    }
    override func viewWillDisappear(_ animated: Bool) {
        lines.removeAll()
        strokeWidth = 8.0
    }
    
    @IBAction func btnMagnify(_ sender: UIButton) {
            strokeWidth = 3.0
            scrollView.isScrollEnabled = false
    }
    
    @IBAction func btnShare(_ sender: Any) {
        let img = viewCanvas.toImage()
        guard let imageData = img?.jpegData(compressionQuality: 1.0) else {
            return
        }
        let activityViewController = UIActivityViewController(activityItems: [imageData], applicationActivities: nil)
        present(activityViewController, animated: true, completion: nil)
    }
    
    @IBAction func btnReset(_ sender: Any) {
        lines.removeAll()
        viewDrawPad.setNeedsDisplay()
    }
    
    @IBAction func btnUndu(_ sender: UIButton) {
        if lines.count > 0{
            lines.removeLast()
            viewDrawPad.setNeedsDisplay()
        }
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
extension ChitraVC: UIScrollViewDelegate{
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        // Return the view that you want to zoom
        return scrollView.subviews.first
    }
    
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        // Perform any necessary updates or adjustments after zooming
    }
    
}

