//
//  Store.swift
//  GujGuru
//
//  Created by Nick Joliya on 12/10/23.
//
import Foundation

class GujaratiGyanManager {
    static let shared = GujaratiGyanManager() // Singleton instance
    
    struct Item {
        let title: String
        let description: String
        let imageURL: String // You can use URL type if the images are online
        
        init(title: String, description: String, imageURL: String) {
            self.title = title
            self.description = description
            self.imageURL = imageURL
        }
    }
    
    struct Article {
        let title: String
        let image: String
        let desc: String // You can use URL type if the images are online
        
        init(title: String, image: String, description: String) {
            self.title = title
            self.image = image
            self.desc = description
        }
    }
    
    let allTopics: [Article]  = [
        Article(title: "ચિત્ર દોરો", image: "image1", description: "Description for Article 1"),
        Article(title: "ક  ખ ગ ", image: "image2", description: "Description for Article 2"),
        Article(title: "1 થી 9", image: "image1", description: "Description for Article 1"),
        Article(title: "ટપકા જોડી ચિત્ર પુરુ કરો ", image: "image2", description: "Description for Article 2"),
        Article(title: "GujGuru એપ વિશે", image: "image1", description: "Description for Article 1"),
        Article(title: "GujGuru માં સંપર્ક કરો", image: "image2", description: "Description for Article 2"),
        // Add more featured articles here
    ]
    
    let featuredArticles: [Article]  = [
        Article(title: "Article Title 1", image: "image1", description: "Description for Article 1"),
        Article(title: "Article Title 2", image: "image2", description: "Description for Article 2"),
        // Add more featured articles here
    ]
    
    let trendingTopics: [Article]  = [
        Article(title: "Trending Topic 1", image: "image3", description: "Description for Trending Topic 1"),
        Article(title: "Trending Topic 2", image: "image4", description: "Description for Trending Topic 2"),
        // Add more trending topics here
    ]
    
    let articlesForLearning: [Article]  = [
        Article(title: "Learning Article 1", image: "image5", description: "Description for Learning Article 1"),
        Article(title: "Learning Article 2", image: "image6", description: "Description for Learning Article 2"),
        // Add more articles for learning here
    ]
    
    let externalResources: [Article]  = [
        Article(title: "External Resource 1", image: "image7", description: "Description for External Resource 1"),
        Article(title: "External Resource 2", image: "image8", description: "Description for External Resource 2"),
        // Add more external resources here
    ]
    
    let menuItems: [Item] = [
        Item(title: "Featured Articles", description: "Description for Featured Articles", imageURL: "https://example.com/item1.jpg"),
        Item(title: "Trending Topics", description: "Description for Trending Topics", imageURL: "https://example.com/item2.jpg"),
        Item(title: "Articles for Learning", description: "Description for Articles for Learning", imageURL: "https://example.com/item3.jpg"),
        Item(title: "External Resources", description: "Description for External Resources", imageURL: "https://example.com/item4.jpg"),
        // Add more menu items here
    ]
   
    private init() { } // Private constructor to enforce singleton pattern
}
