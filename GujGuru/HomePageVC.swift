//
//  HomePageVC.swift
//  GujGuru
//
//  Created by Nick Joliya on 12/10/23.
//

import UIKit


class HomePageVC: UIViewController {

    @IBOutlet weak var cvMainList: UICollectionView!
    var data : [GujaratiGyanManager.Article] = []
    override func viewDidLoad() {
        super.viewDidLoad()

        data = GujaratiGyanManager.shared.allTopics
        cvMainList.delegate = self
        cvMainList.dataSource = self
    }
}

extension HomePageVC : UICollectionViewDataSource ,UICollectionViewDelegate , UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CVC_Dashboard", for: indexPath) as! CVC_Dashboard
        cell.lblTitle.text = data[indexPath.row].title
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: cvMainList.frame.size.width , height: 120)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChitraVC") as! ChitraVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
            else if indexPath.row == 1 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "KaKhaGaVC") as! KaKhaGaVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if indexPath.row == 2 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "_thi10VC") as! _thi10VC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if indexPath.row == 3 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "JoinPointToDrowVC") as! JoinPointToDrowVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if indexPath.row == 4 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "aboutVC") as! aboutVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if indexPath.row == 5 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ContactVC") as! ContactVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
//       else if indexPath.row == 6 {
//            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AboutusVC") as! AboutusVC
//            self.navigationController?.pushViewController(vc, animated: true)
//        }else if indexPath.row == 7 {
//            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ContactUsVC") as! ContactUsVC
//            self.navigationController?.pushViewController(vc, animated: true)
//        }else if indexPath.row == 8 {
//            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PrivacyVC") as! PrivacyVC
//            self.navigationController?.pushViewController(vc, animated: true)
//        }
        
    }
}

