//
//  KaKhaGaVC.swift
//  GujGuru
//
//  Created by Nick Joliya on 12/10/23.
//

import UIKit
class KaKhaGaVC: UIViewController {

    @IBOutlet weak var viewCanvas: UIView!
    
    @IBOutlet weak var lbl: UILabel!
    
    @IBOutlet weak var viewDrawPad: CanvasView!
    
    let kakhaga = "કખગઘઙચછજઝઞટઠડઢણતથદધનપફબભમયરલવશષસહળક્ષ"
    
    var curruntIndex = 0{
        didSet{
            let index = kakhaga.index(kakhaga.startIndex, offsetBy: curruntIndex)
            let character = kakhaga[index]
            lbl.text = "\(character)"
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        strokeColor = .white
        strokeWidth = 10.0
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        strokeColor = .black
        lines.removeAll()
        strokeWidth = 3.0
    }

    @IBAction func btnShare(_ sender: Any) {
        let img = viewCanvas.toImage()
        let activityViewController = UIActivityViewController(activityItems: [img!], applicationActivities: nil)
        present(activityViewController, animated: true, completion: nil)
    }
    
    
    @IBAction func btnReset(_ sender: Any) {
        lines.removeAll()
        viewDrawPad.setNeedsDisplay()
//        viewDidLoad()
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
        lines.removeAll()
        viewDrawPad.setNeedsDisplay()
        if curruntIndex > 0{
            curruntIndex -= 1
        }else{
            curruntIndex = kakhaga.count-1
        }
    }
    
    @IBAction func btnNext(_ sender: UIButton) {
        lines.removeAll()
        viewDrawPad.setNeedsDisplay()
        if curruntIndex < kakhaga.count-1{
            curruntIndex += 1
        }else{
            curruntIndex = 0
        }
    }
    
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

