//
//  JoinPointToDrowVC.swift
//  GujGuru
//
//  Created by Nick Joliya on 13/10/23.
//

import UIKit
class JoinPointToDrowVC: UIViewController {

    @IBOutlet weak var viewCanvas: UIView!
    
    @IBOutlet weak var imgMain: UIImageView!
    
    @IBOutlet weak var droeViw: CanvasView!
    
    
    var curruntIndex = 1{
        didSet{
            imgMain.image = UIImage(named: "d\(curruntIndex)")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        strokeColor = .black
        strokeWidth = 1.0
        curruntIndex = 1
        showAlertWithInput(str: "Tap On Leading And Trailing Of Image To Go Next Image Or Previes One.")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        strokeColor = .black
        lines.removeAll()
        strokeWidth = 8.0
    }

    func showAlertWithInput(str: String){
        let alertController = UIAlertController(title: str, message: "", preferredStyle: .alert)
        let saveAction = UIAlertAction(title: "OK", style: .default, handler: { alert -> Void in
        })
//        alertController.view.backgroundColor = .black
        alertController.addAction(saveAction)
        present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func btnShare(_ sender: Any) {
        let img = viewCanvas.toImage()
        let activityViewController = UIActivityViewController(activityItems: [img!], applicationActivities: nil)
        present(activityViewController, animated: true, completion: nil)
    }
    
    @IBAction func btnReset(_ sender: Any) {
        lines.removeAll()
        droeViw.setNeedsDisplay()
//        viewDidLoad()
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
        lines.removeAll()
        droeViw.setNeedsDisplay()
        if curruntIndex > 1{
            curruntIndex -= 1
        }else{
            curruntIndex = 14
        }
    }
    
    @IBAction func btnNext(_ sender: UIButton) {
        lines.removeAll()
        droeViw.setNeedsDisplay()
        if curruntIndex < 6{
            curruntIndex += 1
        }else{
            curruntIndex = 1
        }
    }
    
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

