//
//  viewExtension.swift
//  GymGenie
//
//  Created by Apple on 03/05/23.
//

import Foundation
import UIKit
import ImageIO

@IBDesignable extension UIView {
    
    @IBInspectable var cornerRadius: CGFloat {
        get { return layer.cornerRadius }
        set {
            layer.cornerRadius = newValue
            
            // If masksToBounds is true, subviews will be
            // clipped to the rounded corners.
            layer.masksToBounds = (newValue > 0)
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        get {
            guard let cgColor = layer.borderColor else {
                return nil
            }
            return UIColor(cgColor: cgColor)
        }
        set { layer.borderColor = newValue?.cgColor }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable var shadowColor: UIColor {
        get {
            return UIColor(cgColor: layer.shadowColor!)
        }
        set {
            layer.shadowColor = newValue.cgColor
        }
    }
    
    @IBInspectable var shadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        set {
            layer.shadowOpacity = newValue
        }
    }
    
    @IBInspectable var shadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        set {
            layer.shadowOffset = newValue
        }
    }
    
    @IBInspectable var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
        }
    }
    
    func addShadow() {
        layer.shadowColor = shadowColor.cgColor
        layer.shadowOpacity = shadowOpacity
        layer.shadowOffset = shadowOffset
        layer.shadowRadius = shadowRadius
    }
    
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.

fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l < r
    case (nil, _?):
        return true
    default:
        return false
    }
}


//MARK: Add Gif in image

extension UIImage {
    
    public class func gifImageWithData(_ data: Data) -> UIImage? {
        guard let source = CGImageSourceCreateWithData(data as CFData, nil) else {
            print("image doesn't exist")
            return nil
        }
        
        return UIImage.animatedImageWithSource(source)
    }
    
    public class func gifImageWithURL(_ gifUrl:String) -> UIImage? {
        guard let bundleURL:URL = URL(string: gifUrl)
        else {
            print("image named \"\(gifUrl)\" doesn't exist")
            return nil
        }
        guard let imageData = try? Data(contentsOf: bundleURL) else {
            print("image named \"\(gifUrl)\" into NSData")
            return nil
        }
        
        return gifImageWithData(imageData)
    }
    
    public class func gifImageWithName(_ name: String) -> UIImage? {
        guard let bundleURL = Bundle.main
            .url(forResource: name, withExtension: "gif") else {
            print("SwiftGif: This image named \"\(name)\" does not exist")
            return nil
        }
        guard let imageData = try? Data(contentsOf: bundleURL) else {
            print("SwiftGif: Cannot turn image named \"\(name)\" into NSData")
            return nil
        }
        
        return gifImageWithData(imageData)
    }
    
    class func delayForImageAtIndex(_ index: Int, source: CGImageSource!) -> Double {
        var delay = 0.1
        
        let cfProperties = CGImageSourceCopyPropertiesAtIndex(source, index, nil)
        let gifProperties: CFDictionary = unsafeBitCast(
            CFDictionaryGetValue(cfProperties,
                                 Unmanaged.passUnretained(kCGImagePropertyGIFDictionary).toOpaque()),
            to: CFDictionary.self)
        
        var delayObject: AnyObject = unsafeBitCast(
            CFDictionaryGetValue(gifProperties,
                                 Unmanaged.passUnretained(kCGImagePropertyGIFUnclampedDelayTime).toOpaque()),
            to: AnyObject.self)
        if delayObject.doubleValue == 0 {
            delayObject = unsafeBitCast(CFDictionaryGetValue(gifProperties,
                                                             Unmanaged.passUnretained(kCGImagePropertyGIFDelayTime).toOpaque()), to: AnyObject.self)
        }
        
        delay = delayObject as! Double
        
        if delay < 0.1 {
            delay = 0.1
        }
        
        return delay
    }
    
    class func gcdForPair(_ a: Int?, _ b: Int?) -> Int {
        var a = a
        var b = b
        if b == nil || a == nil {
            if b != nil {
                return b!
            } else if a != nil {
                return a!
            } else {
                return 0
            }
        }
        
        if a < b {
            let c = a
            a = b
            b = c
        }
        
        var rest: Int
        while true {
            rest = a! % b!
            
            if rest == 0 {
                return b!
            } else {
                a = b
                b = rest
            }
        }
    }
    
    class func gcdForArray(_ array: Array<Int>) -> Int {
        if array.isEmpty {
            return 1
        }
        
        var gcd = array[0]
        
        for val in array {
            gcd = UIImage.gcdForPair(val, gcd)
        }
        
        return gcd
    }
    
    class func animatedImageWithSource(_ source: CGImageSource) -> UIImage? {
        let count = CGImageSourceGetCount(source)
        var images = [CGImage]()
        var delays = [Int]()
        
        for i in 0..<count {
            if let image = CGImageSourceCreateImageAtIndex(source, i, nil) {
                images.append(image)
            }
            
            let delaySeconds = UIImage.delayForImageAtIndex(Int(i),
                                                            source: source)
            delays.append(Int(delaySeconds * 1000.0)) // Seconds to ms
        }
        
        let duration: Int = {
            var sum = 0
            
            for val: Int in delays {
                sum += val
            }
            
            return sum
        }()
        
        let gcd = gcdForArray(delays)
        var frames = [UIImage]()
        
        var frame: UIImage
        var frameCount: Int
        for i in 0..<count {
            frame = UIImage(cgImage: images[Int(i)])
            frameCount = Int(delays[Int(i)] / gcd)
            
            for _ in 0..<frameCount {
                frames.append(frame)
            }
        }
        
        let animation = UIImage.animatedImage(with: frames,
                                              duration: Double(duration) / 1000.0)
        
        return animation
    }
}

extension UIView {
    func toImage() -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(bounds.size, isOpaque, 0.0)
        defer { UIGraphicsEndImageContext() }
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        layer.render(in: context)
        return UIGraphicsGetImageFromCurrentImageContext()
    }
}


//MARK: single touch draw

class SingleTouchDrawView: UIView {
    
    private var path: UIBezierPath?
    private var strokeColor: UIColor = .white
    private var strokeWidth: CGFloat = 2.0
    
    override func draw(_ rect: CGRect) {
        strokeColor.setStroke()
        path?.lineWidth = strokeWidth
        path?.stroke()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let point = touch.location(in: self)
            path = UIBezierPath()
            path?.move(to: point)
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first, let path = path {
            let point = touch.location(in: self)
            path.addLine(to: point)
            setNeedsDisplay()
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        path = nil
    }
    
    func setStrokeColor(_ color: UIColor) {
        strokeColor = color
    }
    
    func setStrokeWidth(_ width: CGFloat) {
        strokeWidth = width
    }
    
    func clear() {
        path = nil
        setNeedsDisplay()
    }
}

//MARK: need to change
//
//class ViewController: UIViewController {
//
//    var penDrawableView: PenDrawableView!
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//        // Create the pen drawable view
//        penDrawableView = PenDrawableView(frame: view.bounds)
//        penDrawableView.backgroundColor = .white
//        view.addSubview(penDrawableView)
//
//        // Customize the stroke color and width
//        penDrawableView.setStrokeColor(.red)
//        penDrawableView.setStrokeWidth(3.0)
//    }
//
//    // Example method to clear the drawing
//    @IBAction func clearButtonTapped(_ sender: UIButton) {
//        penDrawableView.clear()
//    }
//}

//
//@IBDesignable
//class Canvas: UIView {
//    var xBall = ...
//    var yBall = ...
//
//    let shapeLayer: CAShapeLayer = {
//        let shapeLayer = CAShapeLayer()
//        shapeLayer.strokeColor = UIColor.green.cgColor
//        shapeLayer.fillColor = UIColor.clear.cgColor
//        shapeLayer.lineWidth = 2
//        return shapeLayer
//    }()
//
//    override init(frame: CGRect = .zero) {
//        super.init(frame: frame)
//        configure()
//    }
//
//    required init?(coder: NSCoder) {
//        super.init(coder: coder)
//        configure()
//    }
//
//    func configure() {
//        layer.addSublayer(shapeLayer)
//
//        updatePath()
//    }
//
//    func updatePath() {
//        let path = UIBezierPath()
//        path.move(to: CGPoint(x: 30, y: 30))
//        path.addLine(to: CGPoint(x: xBall, y: yBall))
//        shapeLayer.path = path.cgPath
//    }
//}


//MARK: total art sketch

var strokeWidth: CGFloat = 8.0
var strokeColor: UIColor = .black
var lines: [Line] = []

class CanvasView: UIView{
    
//     var strokeWidth: CGFloat = 8.0
//     var strokeColor: UIColor = .black
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        guard let context = UIGraphicsGetCurrentContext() else{ return }
        
        lines.forEach { lines in
            context.setStrokeColor(lines.strokeColor.cgColor)
            context.setLineWidth(lines.strokeWidth)
            context.setLineCap(.round)
            
            for (index, point) in lines.points.enumerated(){
                if index == 0{
                    context.move(to: point)
                }else{
                    context.addLine(to: point)
                }
            }
            context.strokePath()
        }
    }
    
//    #warning("Touch Begun Code")
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let point = touches.first?.location(in: self) else{ return }
        
        let newLine = Line(points: [point], strokeColor: strokeColor, strokeWidth: strokeWidth)
        lines.append(newLine)
        setNeedsDisplay()
    }
    
//    #warning("Draw Line")
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let point = touches.first?.location(in: self) else{ return }
        guard var lastLine = lines.popLast() else{ return }
        
        lastLine.points.append(point)
        lines.append(lastLine)
        setNeedsDisplay()
    }
}


//MARK: Struct for art line

struct Line{
    
    var points: [CGPoint]
    let strokeColor: UIColor
    let strokeWidth: CGFloat
}

